/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import 'react-native-gesture-handler';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Icon from 'react-native-vector-icons/Ionicons';
import { Container, Content, Item, Input } from 'native-base';
import GiftsForm from './components/GiftsForm';

import Meapisearch from './components/Meapisearch';
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,Button,ImageBackground, Image, Dimensions, TouchableOpacity
} from 'react-native';
 
import {
  
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
 
 import Tabss from './components/Tabss';
import Landing from './components/Landing';
import Logins from './components/Logins';
import SearhPerson from './components/SearhPersons';
import SearhPersons from './components/SearhPersons';
import Landingtwo from './components/Landingtwo';
import Logintwo from './components/Logintwo';
import RegThree from './components/RegThree';
import Postandgetmethod from './components/Postandgetmethod';
 
import Mecamera from './components/Mecamera';
import BgImage from './components/Assets/icons/background.jpg';
import Logob from './components/Assets/icons/Logob.png';
import Logot from './components/Assets/icons/Logot.png';
import Logow from './components/Assets/icons/Logow.png';
import { TextInput, } from 'react-native-gesture-handler';
import { Header } from 'react-native-elements'
import TabTwo from './components/TabTwo';
const { width: WIDTH } = Dimensions.get('window')
 
class LogoTitle extends React.Component {
  render() {
    return (
      <Image
        source={Logob}
        style={{ width: 30, height: 30 }}
      />
    );
  }
}

class HomeScreen extends React.Component{
  constructor() {
    super()
    this.state = {
      showPass: true,
      press: false
    }
  }
  showPass = () => {
    if(this.state.press == false){
      this.setState({showPass: false, press: true})
    }
    else{
      this.setState({showPass: true, press: false})
    }
  }
  // static navigationOptions = {
  //   headerTitle: () => <LogoTitle />,
  //   headerRight: () => (
  //     <Button
  //       onPress={() => alert('This is a button!')}
  //       title="Info"
  //       color="#fff"
  //     />
  //   ),
  // };
  render(){
  return (
         
     <ImageBackground source={BgImage} style={styles.BackgroundContainer}>
    
           <View styles={styles.LogoContainer}>
         <Image source={Logob} style={styles.Logob} />
             <Text style={styles.LogText}>Prisons Visitor Management</Text>
           </View>
           <View style={styles.InputContainer}>
             <Icon name={'ios-person'} size={28} color={'rgba(255, 255, 255, 0.7)'}
           style={styles.InputIcon}
             />
             <TextInput
               style={styles.input}
               placeholder={'Username'}
               placeholderTextColor={'rgba(255,255,255, 0.7)'}
               underlineColorAndroid='transparent'
           />
           </View>
           <View style={styles.InputContainer}>
             <Icon name={'ios-lock'} size={28} color={'rgba(255, 255, 255, 0.7)'}
             style={styles.InputIcon}
             />
             <TextInput
               style={styles.input}
               placeholder={'Password'}
               secureTextEntry={this.state.showPass}
               placeholderTextColor={'rgba(255,255,255, 0.7)'}
               underlineColorAndroid='transparent'
             />
             <TouchableOpacity style={styles.BtnEye}
               onPress={this.showPass.bind(this)}>
               <Icon name={this.state.press == false ? 'ios-eye' : 'ios-eye-off'} size={26} color={'rgba(255, 255, 255, 0.7)'} />
             </TouchableOpacity>
           </View>
           <TouchableOpacity style={styles.BtnLogin} onPress={() => this.props.navigation.navigate('Land')}>
             <Text style={styles.BtnText}>Login</Text>
          </TouchableOpacity>

        </ImageBackground>


  );
};
} 
class DetailsScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Details Screen</Text>
        <Button
          title="Go back Home"
          onPress={() => this.props.navigation.navigate('Home')}
        />
      </View>
    );
  }
}
const RootStack = createStackNavigator(
  {
    Home: HomeScreen,
    Details: DetailsScreen,
    Land: Landing,
    Lands: Landingtwo,
    CallAm: SearhPersons,
    Register: Logins,
    TwoLogin: Logintwo,
    RegTu: RegThree,
    Came: Mecamera,
    RealTab: Tabss,
    Tab22: TabTwo,
  },
  {
    initialRouteName: 'Home',
  
  defaultNavigationOptions: {
  headerStyle: {
    backgroundColor: '#17172A',
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
},
  }
);

const AppContainer = createAppContainer(RootStack);

const styles = StyleSheet.create({
  BackgroundContainer: {
    flex: 1,
    width: 450,
     
    height: 800,

    justifyContent: 'center',
    alignItems: 'center'
  },
  Logob: {
    margin: 20,
    marginTop: -80,
    marginLeft: 45,
    width: 120,
    height: 120,
  },
  LogoContainer: {
    marginBottom: 40,

    alignItems: 'center',
  },
  LogText: {
    color: 'white',
    fontSize: 20,
    fontWeight: '500',
    marginTop: 10,
    opacity: 0.5,
  },
  InputContainer: {
    margin: 10,
  },
  input: {
    width: WIDTH - 55,
    height: 45,
    borderRadius: 45,
    fontSize: 16,
    paddingLeft: 45,
    backgroundColor: 'rgba(0,0,0, 0.35)',
    color: 'rgba(255, 255, 255, 0.7)',
    marginHorizontal: 25,
  },
  InputIcon: {
    position: 'absolute',
    top: 8,
    left: 37,
  },
  BtnEye: {
    position: 'absolute',
    top: 8,
    right: 37,
  },
  BtnLogin: {
    width: WIDTH - 55,
    height: 45,
    borderRadius: 45,
    backgroundColor: '#432577',
    justifyContent: 'center',
    marginTop: 20
  },
  BtnText: {
    color: 'rgba(255, 255, 255, 0.7)',
    fontSize: 16,
    textAlign: 'center'
  },
  Btn: {
    backgroundColor: '#ffffff',
    borderRadius: 60,
    width: 100,
    alignSelf: 'center',
    marginTop: 20,
    paddingTop: 10,
    paddingLeft: 10,
    alignContent: 'center',
  },
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}
