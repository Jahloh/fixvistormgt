import React, { Component } from 'react';
import { View, StyleSheet,Button } from 'react-native';
import { Container, Header,List, Content,Fab, ListItem, Text, Icon, Left, Body, Right, Switch } from 'native-base';
import t from 'tcomb-form-native'; // 0.6.9
import App from '../App';
import Mecamera from './Mecamera'
const Form = t.form.Form;

const User = t.struct({
  FirstName: t.String,
  LastName: t.String,
  Address: t.String,
  //tags: t.list(t.String),
  PhoneNumber: t.Number,
  birthDate: t.Date,
  //terms: t.Boolean,
});

const options = {
    fields: {
         
        FirstName: {
            auto: 'none',
            placeholder: 'Enter Fist Name name here',
            error:
          'A Valid Address is needed',
        },  
      LastName: {
        auto: 'none',
            placeholder: 'Enter Last Name name here',
        error:
          'Please enter your Last Name',
      },
      Address: {
        auto: 'none',
            placeholder: 'Enter Last Address here',
        error:
          'A Valid Address is needed',
      },
      PhoneNumber: {
          auto: 'none',
        placeholder: 'Enetr your Phone numeber here',
        error:
          "A Phone Number Needs to be entered",
      },
      terms: {
        label: 'Agree to Terms',
      },
      birthDate: {
        mode: 'date' // display the Date field as a DatePickerAndroid
      }
    },
  };

export default class RegThree extends Component {
  constructor(props) {
    super(props)
    this.state = {
      active: false
    };
  }
  clearForm() {
    // clear content from all textbox
    this.setState({ value: null });
  }

    handleSubmit = () => {
        const value = this._form.getValue(); // use that ref to get the form value
    console.log('value: ', value)
    //Na from ya r put am
   // var value = this.refs.form.getValue();
  if (value) { // if validation fails, value will be null
    fetch("http://192.168.137.47:4000/router/all-data", {
      method: "POST",
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        title: value.title,
        genre:value.genre,
      }),
    })
    .then((response) => response.json())
    .then((responseJson) => {
      
      return responseJson.all-data;
    })
    .catch((error) => {
      console.error(error);
    });
  }

    // na ya en end
    //option two
//     fetch('https://mywebsite.com/endpoint/', {
//   method: 'POST',
//   headers: {
//     Accept: 'application/json',
//     'Content-Type': 'application/json',
//   },
//   body: JSON.stringify({
//     firstParam: 'yourValue',
//     secondParam: 'yourOtherValue',
//   }),
// }).then((response) => response.json())
//     .then((responseJson) => {
//       return responseJson.movies;
//     })
//     .catch((error) => {
//       console.error(error);
//     });
    //option two end ya
    this.clearForm();
      };
      
  render() {
    return (
      <Container>
      <View style={styles.container}> 
        <Form
          ref={c => (this._form = c)} // assign a ref
          type={User}
          options={options}
        />
         
        <Button title=" Send" onPress={this.handleSubmit} />
        
      </View>
      <Fab
            active={this.state.active}
            direction="up"
            containerStyle={{ }}
            style={{ backgroundColor: '#5067FF' }}
            position="bottomRight"
            onPress={() => this.props.navigation.navigate('Came')}>
            <Icon name="camera" />
             
             
          </Fab>
          </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    marginTop: 0,
    padding: 20,
    backgroundColor: '#ffffff',
  },
  
});