import React, { Component } from 'react';
import {
  AppRegistry,
  Dimensions,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  StatusBar,
  TouchableOpacity,
  Image
} from 'react-native';
import Camera from 'react-native-camera';


export default class Photo extends Component {
  static navigationOptions = ({navigation}) => {
        const {params = {}} = navigation.state;
        return {
            title: 'Take a Photo',
            headerTitleStyle:{ color: '#000000',fontFamily:'Montserrat-Regular', alignSelf: 'center' },
        };
    };

  constructor(props) {
    super(props);

    this.camera = null;

    this.state = {
      camera: {
        captureQuality: Camera.constants.CaptureQuality.low,
        captureTarget: Camera.constants.CaptureTarget.cameraRoll,
        type: Camera.constants.Type.front,
        preview: Camera.constants.CaptureQuality.preview,
		    flashMode: Camera.constants.FlashMode.on,
        orientation: Camera.constants.Orientation.portrait,
        playSoundOnCapture: false,
      },
      isRecording: false
    };
  }

  takePicture() {
    const { params }  = this.props.navigation.state;

    //Map from previous form
    var name = params.name;
    var contactNumber = params.contactNumber;
    var email = params.email;
    var vehicleNumber = params.vehicleNumber;
    var organization = params.organization;

    // var purposeOfVisit = params.purposeOfVisit;
    // var staffName = params.staffName;

    if (this.camera) {
      this.camera.capture()
        .then((data) => 
          this.props.navigation.navigate('Signature', { name: name, 
                           'contactNumber': contactNumber ,
                             'email': email,
                            'vehicleNumber': vehicleNumber,
                            'organization': organization,
                            'image': data.path,
                            
                            // 'purposeOfVisit': purposeOfVisit,
                            // 'staffName': staffName,
                          }))
        .catch(err => console.error(err));

        
    }
  }


  render() {
    const { params } = this.props.navigation.state;
    const { navigate } = this.props.navigation;

    return (
      <View style={styles.container}>
        <StatusBar
          animated
          hidden
        />
        <Camera
          ref={(cam) => {
            this.camera = cam;
          }}
          style={styles.preview}
          aspect={this.state.camera.aspect}
          captureTarget={this.state.camera.captureTarget}
          type={this.state.camera.type}
          playSoundOnCapture={this.state.camera.playSoundOnCapture}
          orientation={this.state.camera.orientation}
		      flashMode={this.state.camera.flashMode}
          mirrorImage={false}
        />
        <View style={[styles.overlay, styles.topOverlay]}>
          <TouchableOpacity
            style={styles.typeButton}
            onPress={this.switchType}
          >
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.flashButton}
            onPress={this.switchFlash}
          >
            <Image
              source={this.flashIcon}
            />
          </TouchableOpacity>
        </View>
        <View style={[styles.overlay, styles.bottomOverlay]}>
          {
            !this.state.isRecording
            &&
            <TouchableOpacity
                style={styles.captureButton}
                onPress={this.takePicture.bind(this)}
            >
              <Image
                  source={require('../components/Assets/icons/ic_photo_camera_36pt.png')}
              />
            </TouchableOpacity>
            ||
            null
          }
          <View style={styles.buttonsSpace} />
         
        </View>
      </View>
    );
  }

 
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  overlay: {
    position: 'absolute',
    padding: 16,
    right: 0,
    left: 0,
    alignItems: 'center',
  },
  topOverlay: {
    top: 0,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  bottomOverlay: {
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,0.4)',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  captureButton: {
    padding: 15,
    backgroundColor: 'white',
    borderRadius: 40,
  },
  typeButton: {
    padding: 5,
  },
  flashButton: {
    padding: 5,
  },
  buttonsSpace: {
    width: 10,
  },
});

 
