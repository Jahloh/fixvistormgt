import React, { Component } from 'react';
import { View, StyleSheet,Button } from 'react-native';

import t from 'tcomb-form-native'; // 0.6.9
const Country = t.enums({
    'IT': 'Italy',
    'US': 'United States'
  }, 'Country');
   
  export default VisitorType = React.Component({
   
    // returns the suitable type based on the form value
    getType(value) {
      if (value.country === 'IT') {
        return t.struct({
          country: Country,
          rememberMe: t.Boolean
        });
      } else if (value.country === 'US') {
        return t.struct({
          country: Country,
          name: t.String
        });
      } else {
        return t.struct({
          country: Country
        });
      }
    },
   
    getInitialState() {
      const value = {};
      return { value, type: this.getType(value) };
    },
   
    onChange(value) {
      // recalculate the type only if strictly necessary
      const type = value.country !== this.state.value.country ?
        this.getType(value) :
        this.state.type;
      this.setState({ value, type });
    },
   
    onPress() {
      var value = this.refs.form.getValue();
      if (value) {
        console.log(value);
      }
    },
   
    render() {
   
      return (
        <View style={styles.container}>
          <t.form.Form
            ref="form"
            type={this.state.type}
            value={this.state.value}
            onChange={this.onChange}
          />
          <TouchableHighlight style={styles.button} onPress={this.onPress} underlayColor='#99d9f4'>
            <Text style={styles.buttonText}>Save</Text>
          </TouchableHighlight>
        </View>
      );
    }
  });
