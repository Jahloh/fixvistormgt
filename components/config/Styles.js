import { StyleSheet } from 'react-native';

var Style = StyleSheet.create({

    rootContainer: {
        flex: 1,
        backgroundColor: '#eae8e8'
    },

    buttonHome: {
        backgroundColor: '#E74C3C'
    },
    buttonExit: {
        backgroundColor: '#fff',
    },

    formContainer: {
        flex: 1,
        backgroundColor: '#eae8e8',
        paddingHorizontal: 20,
        paddingVertical: 40,
    },

    homeHeaderContainer: {
        alignItems: 'center'
    },

    homeHeader: {
        paddingVertical: 20,
        fontSize: 40,
        color: '#566573'
    },

    homeLogoContainer: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center'
    },



    button: {
        height: 36,
        width: '90%',
        backgroundColor: '#E74C3C',
        borderColor: '#E74C3C',
        borderWidth: 1,
        borderRadius: 8,
        marginBottom: 10,
        alignSelf: 'center',
        justifyContent: 'center',
        marginTop: 20
    },

    homeBtn: {
        height: 36,
        width: '40%',
        backgroundColor: '#E74C3C',
        borderColor: '#E74C3C',
        borderWidth: 1,
        borderRadius: 8,
        marginBottom: 10,
        alignSelf: 'center',
        justifyContent: 'center',
        marginLeft: 15
    },

    signoutBtn: {
        height: 36,
        width: '40%',
        backgroundColor: '#E74C3C',
        borderColor: '#E74C3C',
        borderWidth: 1,
        borderRadius: 8,
        marginBottom: 10,
        alignSelf: 'center',
        justifyContent: 'center',
        marginRight: 15
    },

    headerRight: {
        color: 'red',
    },

    buttonText: {
        fontSize: 18,
        color: 'white',
        alignSelf: 'center'
    },

    container: {
        justifyContent: 'center',
        padding: 40,
        backgroundColor: '#ffffff'
   },

    title: {
        fontSize: 20,
        alignSelf: 'center',
        marginBottom: 30
   },

   /* Details Page */
    detailContainer: {
        flex: 1,
        backgroundColor: 'white',
        padding: 5
    },

    detailName: {
        color: '#D20A4B',
        fontSize: 24,
        padding:10,
        textAlign:'center',
        marginTop:10
    },

    detailTextTitle:{
        color:'black',
        fontSize:18,
        textAlign:'center',
        fontWeight: 'bold'
    },

    detailTextValue:{
        color:'black',
        fontSize:18,
        textAlign:'center'
    },

    clearText: {
        fontSize:25,
    },

    clearContainer: {
         alignItems: 'center',
    },

    detailDirection:{
        alignItems: 'flex-start',
        flexDirection: 'row',
        padding: 5
    },

    detailImgDirection:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'stretch',
    },


    contentContainer: {
        paddingHorizontal: 30,
        paddingVertical: 15
    },

    scanContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },

    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },

    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    rectangleContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'transparent',
    },

    rectangle: {
        height: 250,
        width: 250,
        borderWidth: 2,
        borderColor: '#00FF00',
        backgroundColor: 'transparent',
    },  

    homeBtnContainer: {
        flexDirection:'row',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 0,
    },

    homeScanBtnText: {
        fontSize: 18,
        color: 'white',
        alignSelf: 'center'
    },


    backBtn: {
        height: 36,
        width: '50%',
        backgroundColor: '#E74C3C',
        borderColor: '#E74C3C',
        borderWidth: 1,
        borderRadius: 8,
        marginBottom: 10,
        alignSelf: 'center',
        justifyContent: 'center',
        marginTop: 5
    },



    });

export default Style;