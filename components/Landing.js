import React, { Component } from 'react';
import 'react-native-gesture-handler';
import RegThree from './RegThree';
import SearhPersons from './SearhPersons';
import App from '../App';
import Logins from './Logins';
import Meapisearch from './Meapisearch';
import {Image} from 'react-native';
import Logob from './Assets/icons/Logob.png';
import NewApiwithPhoto from './NewApiwithPhoto';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Container, Header,View,List, Content, Button,Fab, ListItem, Text, Icon, Left, Body, Right, Switch } from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
import VisitorType from './VisitorType';

class LogoTitle extends React.Component {
  render() {
    return (
      <View>
      <Image
        source={Logob}
        style={{ width: 30, height: 30 }}
      />
        <Text style={{ textAlign: 'center' }}>Landing Page</Text>
      
      </View>
      
    );
  }
}
export default class Landing extends Component {
  constructor(props) {
    super(props)
    this.state = {
      active: false
    };
  }

  static navigationOptions = {
    headerTitle: () => <LogoTitle />,
    headerRight: () => (
      <Button
        onPress={() => alert('This is a button!')}
        title="Info"
        color="#fff"
      />
    ),
  };

  render() {
    return (  
        
      <Container>
      
      
       <Meapisearch/>  
        
        
          <Fab
            active={this.state.active}
            direction="up"
            containerStyle={{ }}
            style={{ backgroundColor: '#5067FF' }}
            position="bottomRight"
            onPress={() => this.setState({ active: !this.state.active })}>
            <Icon name="add" />
            <Button style={{ backgroundColor: '#34A34F' }} onPress={() => this.props.navigation.navigate('Register')}>
              <Icon name="search"  />
              
            </Button>
            <Button style={{ backgroundColor: '#3B5998' }} onPress={() => this.props.navigation.navigate('CallAm')}>
              <Icon name="eye" />
            </Button>
            <Button style={{ backgroundColor: '#DD5144' }} onPress={() => this.props.navigation.navigate('RealTab')}>
              <Icon name="person" />
            </Button>
             
          </Fab>
         
      </Container>
     
    );
  }
}

