import React, { Component } from 'react';
import {
   Platform,
   StyleSheet,
   Text,
   View,
   ScrollView,
   TextInput,
   FlatList,
   Alert,
   TouchableOpacity,
} from 'react-native';

import Image from 'react-native-image-progress';
import ProgressBar from 'react-native-progress';

type Props = {};
export default class NewApiwithPhoto extends Component<Props> {

constructor(props) {
  super(props);
   this.getListCall= this.getListCall.bind(this);
   this.GetListItem= this.GetListItem.bind(this);
   this.state = { 
    JSONResult: "",
   }
}

componentDidMount(){
  this.getListCall();
}

getListCall(){
const that = this;
const url = "http://localhost:4000/router/all-data";
console.log("-----------url:"+url);
fetch(url,{method: 'GET'}).then(function (response) {
    return response.json();
  }).then(function (result) {

  if(result.status.response === "success"){
   that.setState({ 
     JSONResult: result.all-data,
   });
  }
console.log(result.all-data);
 }).catch(function (error) {
   console.log("-------- error ------- "+error);
  alert("result:"+error)
 });
}

GetListItem (name) {
  Alert.alert(name);
}

ItemSeparatorLine = () => {
  return (
    <View
    style={{height: .5,width: "100%",backgroundColor: "#111a0b",}}
    />
  );
}

render() {
  return ( 
    <View style={styles.container}>
    <FlatList
      data={ this.state.JSONResult.movies }
      ItemSeparatorComponent = {this.ItemSeparatorLine}

  renderItem={({item}) => 
   <TouchableOpacity activeOpacity={0.9} onPress={this.GetListItem.bind(this, item.title)}>
   <View style={styles.container} >

   <Image 
     source={{ uri: item.poster }} 
     indicator={ProgressBar} 
     indicatorProps={{
     size: 80,
     borderWidth: 0,
     color: 'rgba(150, 150, 150, 1)',
     unfilledColor: 'rgba(200, 200, 200, 0.2)'
   }}
  style={{
     width: 320, 
     height: 240, 
     alignItems: 'center',
     justifyContent: 'center',
 }}/>


   <Text style={styles.welcome} > {item.title} </Text>
   </View>
</TouchableOpacity>
}
keyExtractor={(item, index) => index}
/>
</View> 
);
}
}

const styles = StyleSheet.create({
container: {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: '#FFFFFF',
},
welcome: {
  fontSize: 20,
  textAlign: 'center',
  margin: 10,
},
instructions: {
  textAlign: 'center',
  color: '#333333',
  marginBottom: 5,
},
});