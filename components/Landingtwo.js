// import React, { Component } from 'react';
// import 'react-native-gesture-handler';


// import SearhPersons from './SearhPersons';
// import App from '../App';
// import Logins from './Logins';

// import { StyleSheet, Image } from 'react-native';
// import { createAppContainer } from 'react-navigation';
// import { createStackNavigator } from 'react-navigation-stack';
// import { Container, Header, View, List, SafeAreaView, FlatList, Content, Button, Fab, ListItem, Text, Icon, Left, Body, Right, Switch } from 'native-base';
// import { ScrollView } from 'react-native-gesture-handler';
 

// export default class Landingtwo extends Component {
//     constructor(props) {
//         super(props)
//         this.state = {
//             dataSource: [],
//             active: false
//         };
//     }

//     seniRendering = ({ item }) => {
//         return (
//             <View>
//                 <Image style={{width: 100, height: 100}}
//                     source={{ uri: item.poster }} />
//                 <View>
//                     <Text>
//                         {item.title}
//                     </Text>
//                 </View>


//             </View>
//         )


//     }
//     componentDidMount() {
//         const url = 'http://localhost:4000/router/all-data'
//         fetch(url)
//             .then((Response) => Response.json())
//             .then((responseJson) => {
//                 this.setState({
//                     dataSource: responseJson.title
//                 })
//             })
//             .catch((error) => {
//                 console.log(error)
//             })
//     }
//     render() {
//         return (

//             <SafeAreaView style={styles.container}>
//                 <View>
//                     <FlatList
//                         data={this.state.dataSource}
//                         renderItem={this.seniRendering}
//                         keyExtractor={item => item.id}
//                     />
//                 </View>
//                     <Fab
//                     active={this.state.active}
//                     direction="up"
//                     containerStyle={{}}
//                     style={{ backgroundColor: '#5067FF' }}
//                     position="bottomRight"
//                     onPress={() => this.setState({ active: !this.state.active })}>
//                     <Icon name="add" />
//                     <Button style={{ backgroundColor: '#34A34F' }} onPress={() => this.props.navigation.navigate('CallAm')}>
//                         <Icon name="search" />

//                     </Button>
//                     <Button style={{ backgroundColor: '#3B5998' }} onPress={() => this.props.navigation.navigate('Register')}>
//                         <Icon name="eye" />
//                     </Button>
//                     <Button style={{ backgroundColor: '#DD5144' }} onPress={() => this.props.navigation.navigate('Register')}>
//                         <Icon name="person" />
//                     </Button>

//                 </Fab>
//             </SafeAreaView>





//         );
//     }
// }

// const styles = StyleSheet.create({
//     container: {
//         flex: 1,

//     },
//     item: {
//         backgroundColor: '#f9c2ff',
//         padding: 20,
//         marginVertical: 8,
//         marginHorizontal: 16,
//     },
//     title: {
//         fontSize: 32,
//     },
// });

/////////////////////////////////////////////

import React from 'react';
import { SafeAreaView, View, FlatList, StyleSheet, Text } from 'react-native';
 
import SearhPersons from './SearhPersons';
import App from '../App';
import Logins from './Logins';

//import { StyleSheet, Image } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Container, Header,  List, Content, Button, Fab, ListItem, Icon, Left, Body, Right, Switch } from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';

const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'First Item',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'Second Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'Third Item',
  },
];

function Item({ title }) {
  return (
    <View style={styles.item}>
      <Text style={styles.title}>{title}</Text>
    </View>
  );
}

export default function Landingtwo() {
  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={DATA}
        renderItem={({ item }) => <Item title={item.title} />}
        keyExtractor={item => item.id}
      />
      
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});
