/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import 'react-native-gesture-handler';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Container, Content, Item, Input } from 'native-base';

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar, Button, ImageBackground, Image, Dimensions, TouchableOpacity
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Icon from 'react-native-vector-icons/Ionicons';
import Landing from './Landing';
import Logins from './Logins';
//import SearhPerson from './components/SearhPersons';
import SearhPersons from './SearhPersons';
import Landingtwo from './Landingtwo';
import BgImage from './Assets/icons/background.jpg';
import Logob from './Assets/icons/Logob.png';
import Logot from './Assets/icons/Logot.png';
import Logow from './Assets/icons/Logow.png';
import { TextInput, } from 'react-native-gesture-handler';
const { width: WIDTH } = Dimensions.get('window')

export default class Logintwo extends React.Component {
  constructor() {
    super()
    this.state = {
      showPass: true,
      press: false
    }
  }
  showPass = () => {
    if(this.state.press == false){
      this.setState({showPass: false, press: true})
    }
    else{
      this.setState({showPass: true, press: false})
    }
  }
  render() {
    return (
      <ImageBackground source={BgImage} style={styles.BackgroundContainer}>
        <View styles={styles.LogoContainer}>
          <Image source={Logob} style={styles.Logob} />
          <Text style={styles.LogText}>Prisons Visitor Management</Text>
        </View>
        <View style={styles.InputContainer}>
          <Icon name={'ios-person'} size={28} color={'rgba(255, 255, 255, 0.7)'}
            style={styles.InputIcon}
          />
          <TextInput
            style={styles.input}
            placeholder={'Username'}
            placeholderTextColor={'rgba(255,255,255, 0.7)'}
            underlineColorAndroid='transparent'
          />
        </View>
        <View style={styles.InputContainer}>
          <Icon name={'ios-lock'} size={28} color={'rgba(255, 255, 255, 0.7)'}
            style={styles.InputIcon}
          />
          <TextInput
            style={styles.input}
            placeholder={'Password'}
            secureTextEntry={this.state.showPass}
            placeholderTextColor={'rgba(255,255,255, 0.7)'}
            underlineColorAndroid='transparent'
          />
          <TouchableOpacity style={styles.BtnEye}
            onPress={this.showPass.bind(this)}>
            <Icon name={this.state.press == false ? 'ios-eye' : 'ios-eye-off'} size={26} color={'rgba(255, 255, 255, 0.7)'} />
          </TouchableOpacity>
        </View>
        <TouchableOpacity style={styles.BtnLogin}>
          <Text style={styles.BtnText}>Login</Text>
        </TouchableOpacity>

      </ImageBackground>

    );
  };
}

const styles = StyleSheet.create({
  BackgroundContainer: {
    flex: 1,
    width: 425,
    height: 800,

    justifyContent: 'center',
    alignItems: 'center'
  },
  Logob: {
    margin: 20,
    marginTop: -250,
    marginLeft: 45,
    width: 120,
    height: 120,
  },
  LogoContainer: {
    marginBottom: 40,
    alignItems: 'center',
  },
  LogText: {
    color: 'white',
    fontSize: 20,
    fontWeight: '500',
    marginTop: 10,
    opacity: 0.5,
  },
  InputContainer: {
    margin: 10,
  },
  input: {
    width: WIDTH - 55,
    height: 45,
    borderRadius: 45,
    fontSize: 16,
    paddingLeft: 45,
    backgroundColor: 'rgba(0,0,0, 0.35)',
    color: 'rgba(255, 255, 255, 0.7)',
    marginHorizontal: 25,
  },
  InputIcon: {
    position: 'absolute',
    top: 8,
    left: 37,
  },
  BtnEye: {
    position: 'absolute',
    top: 8,
    right: 37,
  },
  BtnLogin: {
    width: WIDTH - 55,
    height: 45,
    borderRadius: 45,
    backgroundColor: '#432577',
    justifyContent: 'center',
    marginTop: 20
  },
  BtnText: {
    color: 'rgba(255, 255, 255, 0.7)',
    fontSize: 16,
    textAlign: 'center'
  },

});


