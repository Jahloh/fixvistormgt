import React, { Component } from 'react';
import firebase from 'firebase'
import { Text, View, StyleSheet, TextInput, TouchableOpacity } from 'react-native'
//import Loading from './Loading';

export class EmailAndPassword extends Component {
    state={
        email: '',
        Password: '',
        error: '',
        loading: false
    }

    onButtomPress= () =>{
        firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.Password)
        .then(this.onLoginSuccess)
        .catch(err => {
            this.setState({
                error:err.message
            })
        })
    }

    onLoginSuccess = () =>{
        this.setState({
            error: '',
            loading: false
        })
    }
    render() {
        return (
            <View style={styles.container}>
            <TextInput
                placeholder ="email" style={styles.inPut}
                value={this.state.email}
                onChangeText={email=> this.setState({email})}
            />
            <TextInput
                placeholder ="Password" style={styles.inPut}
                value={this.state.Password}
                secureTextEntry
                onChangeText={Password=> this.setState({Password})}
            />
            <TouchableOpacity style={styles.ButtonContainer} onPress={this.onButtomPress}>
                <Text style={styles.LoginTText}>Login</Text>
            </TouchableOpacity>
            <Text style={styles.ErrorText}>{this.state.error}</Text>
                 
            </View>
        )
    }
}

const styles= StyleSheet.create({
    container:{
        flex: 1,
        padding:20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    inPut:{
        height:40,
        width: 200,
        backgroundColor: 'rgba(255,255,255,.5)',
        paddingLeft: 10,
        marginBottom: 15,borderRadius:5,
        fontSize: 15,
        backgroundColor: 'white'
    },
    ErrorText:{
        fontSize: 55,
        color: 'red',
        alignSelf: 'center'
    },
    LoginTText:{
        textAlign: 'center',
        color: '#fff',
        fontWeight: "bold",
        fontSize:20,
    },
    ButtonContainer:{
        backgroundColor: '#3B3B98',
        padding: 15,
        borderRadius: 8
    }
     
});
export default EmailAndPassword
