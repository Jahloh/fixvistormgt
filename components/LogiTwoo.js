import React, { Component } from 'react';
import { View, Text, StyleSheet , ImageBackground} from 'react-native';
import LoginForm from './LoginForm'
import Landing from './Landing';
import BG from './Assets/icons/bg.jpeg';
import firebase from 'firebase';
//import Loading from './Loading';
export class LoginTwoo extends Component {

 state={
   loggedIn: null
 }

 componentDidMount(){
    var firebaseConfig = {
        apiKey: "AIzaSyAsuQZcUiTDfg0bEVdUZxrocAmgdC-kHxQ",
        authDomain: "fixprison.firebaseapp.com",
        databaseURL: "https://fixprison.firebaseio.com",
        projectId: "fixprison",
        storageBucket: "fixprison.appspot.com",
        messagingSenderId: "202715843911",
        appId: "1:202715843911:web:9f8f87d7f096a19bd2d062",
        measurementId: "G-86RW3J59D8"
      };
      // Initialize Firebase
      firebase.initializeApp(firebaseConfig);

   firebase.auth().onAuthStateChanged(user=> {
     if(user){
       this.setState({
         loggedIn:true
       })
     } else {
       this.setState({
         loggedIn:false
       })
     }
   })
 }
 renderContent = () =>{
    switch(this.state.loggedIn){
      case false:
        return <ImageBackground style={styles.container} source={BG}>
           <LoginForm/>
        </ImageBackground>

        case  true:
          return <Landing/>

        //   default:
        //     return <Loading/>
    }

 }
  render() {
    return (
      <View style={styles.container}>
         {this.renderContent()}
      </View>
    )
  }
}

const styles=StyleSheet.create({
  container: {
    flex: 1,
    height:'100%',
    width:'100%',
    justifyContent: 'center',
    alignItems: 'center'
  }
});
export default LoginTwoo
