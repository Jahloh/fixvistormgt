import Style from './config/Styles';
import { NavigationActions } from 'react-navigation'
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

var React = require('react');
var ReactNative = require('react-native')

var { AppRegistry, StyleSheet, Text, View, TouchableHighlight, ScrollView } = ReactNative;

import t from 'tcomb-form-native'
import FloatingLabel from 'react-native-floating-label'
import {
  Button
} from 'react-native-elements'



var Form = t.form.Form;

// here we are: define your domain model
const Email = t.subtype(t.Str, (email) => {
  const reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return reg.test(email);
});

const ContactNo = t.subtype(t.Str, (contactNumber) => {
  const reg = /^(\d*)$/;
  return reg.test(contactNumber);
});

const Visitor = t.struct({
  name: t.String,
  organization: t.String,
  email: Email,
  contactNumber: ContactNo,
  vehicleNumber: t.maybe(t.String),

  /*
  purposeOfVisit: t.String, 
  staffName: t.String,
  */

})

var options = {}; // optional rendering options (see documentation)

export default class Registration extends React.Component{
  static navigationOptions = ({navigation}) => {
        const {params = {}} = navigation.state;
        return {
            title: "Guest Information",
            headerTitleStyle:{ color: '#000000',fontFamily:'Montserrat-Regular', alignSelf: 'center' },
        };
    };



  constructor(props) {
    super(props);
    this.state = {
      value: {},
      options: {
        fields: {
          name: {
            factory: FloatingLabel,
            error: 'This field is required.'
          },
          organization: {
            factory: FloatingLabel,
            error: 'This field is required.'
          },
          email: {
            factory: FloatingLabel,
            error: 'Enter a valid email.'
          },
          contactNumber: {
            factory: FloatingLabel,
            error: 'Enter a valid contact number.'
          },
          vehicleNumber: {
            factory: FloatingLabel,
          },
          /*
          purposeOfVisit: {
            factory: FloatingLabel,
            error: 'This field is required.'
          },

          staffName: {
            factory: FloatingLabel,
            error: 'This field is required.'
          },
          */
        },
      },
    }
  }

  // fields to submit:  name, organization, email, contactNumber, vehicleNumber
//    onPress() {
//     // call getValue() to get the values of the form
//     var value = this.refs.form.getValue();
//     if (value) { // if validation fails, value will be null
//       console.log(value); // value here is an instance of Person
//       this.props.navigation.navigate('Photo', { 'name': value.name, 
//                            'contactNumber': value.contactNumber ,
//                              'email': value.email,
//                             'vehicleNumber': value.vehicleNumber,
//                             'organization': value.organization

//                               /*
//                               purposeOfVisit: value.purposeOfVisit, 
//                               staffName: value.purposeOfVisit,
//                               */

//                           })   
//     }
//   }



  render() {
//   const { navigate } = this.props.navigation;
    return (

          <View style={{flex: 1}}>
            <ScrollView style={Style.contentContainer}>
              <Form ref='form'
                type={Visitor}
                value={this.state.value}
                options={this.state.options}
              />
            </ScrollView>


             <Button
              title='NEXT'
              fontSize={20}
              buttonStyle={Style.button}
              fontFamily='Comic Sans MS'
            //   onPress={this.onPress.bind(this)}

              />

          </View>
      
      );
  }


}



