/*This is an Example of SearchBar in React Native*/
import * as React from 'react';
import {  View, StyleSheet, FlatList, ActivityIndicator, ScrollView, Platform } from 'react-native';
import { Container, Header,Fab, Content, List, ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';
import { SearchBar } from 'react-native-elements';
import ProgressBar from 'react-native-progress';
import Image from 'react-native-image-progress';
export default class App extends React.Component {
    constructor(props) {
        super(props);
        //setting default state
        this.state = { isLoading: true, search: '' };
        this.arrayholder = [];
    }
    componentDidMount() {
        return fetch('http://192.168.34.16:4000/router/all-data')
            .then(response => response.json())
            .then(responseJson => {
                this.setState(
                    {
                        isLoading: false,
                        dataSource: responseJson,
                    },
                    function () {
                        this.arrayholder = responseJson;
                    }
                );
            })
            .catch(error => {
                console.error(error);
            });
    }
    search = text => {
        console.log(text);
    };
    clear = () => {
        this.search.clear();
    };
    SearchFilterFunction(text) {
        //passing the inserted text in textinput
        const newData = this.arrayholder.filter(function (item) {
            //applying filter for the inserted text in search bar
            const itemData = item.title ? item.title.toUpperCase() : ''.toUpperCase();
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
        this.setState({
            //setting the filtered newData on datasource
            //After setting the data it will automatically re-render the view
            dataSource: newData,
            search: text,
        });
    }
    ListViewItemSeparator = () => {
        //Item sparator view
        return (
            <View
                style={{
                    height: 0.1,
                    width: '90%',
                    backgroundColor: '#080808',
                }}
            />
        );
    };
    render() {
        if (this.state.isLoading) {
            //Loading View while data is loading
            return (
                <View style={{ flex: 1, paddingTop: 20 }}>
                    <ActivityIndicator />
                </View>
            );
        }
        return (
            //ListView to show with textinput used as search bar
            <View style={styles.viewStyle}>
                <SearchBar
                    round
                    searchIcon={{ size: 24 }}
                    onChangeText={text => this.SearchFilterFunction(text)}
                    onClear={text => this.SearchFilterFunction('')}
                    placeholder="Type Here..."
                    value={this.state.search}
                />
                <FlatList
                    data={this.state.dataSource}
                    ItemSeparatorComponent={this.ListViewItemSeparator}
                    //Item Separator View
                    renderItem={({ item }) => (
                        // Single Comes here which will be repeatative for the FlatListItems

                        //<Text style={styles.textStyle}>{item.title}</Text>
                       /// <Text> {item.title}</Text>
                    <ScrollView>
                        
         
         <Content>
           <List>
             <ListItem thumbnail>
               <Left>
               <Image
                            source={{ uri: item.poster }}
                            indicator={ProgressBar}
                            indicatorProps={{
                                size: 80,
                                borderWidth: 0,
                                color: 'rgba(150, 150, 150, 1)',
                                unfilledColor: 'rgba(200, 200, 200, 0.2)'
                            }}
                            style={{
                                width: 120,
                                height: 140,
                                margin: 10,
                                alignItems: 'center',
                                justifyContent: 'center',
                            }} /> 
              </Left>
               <Body>
               <Text style={styles.textStyle}>{item.title}</Text>
                 <Text note numberOfLines={1}>Its time to build a difference . .</Text>
               </Body>
               <Right>
                 <Button transparent Button  onPress={() => alert('update Visitor Here')}>
                   <Text>Select</Text>
                 </Button>
               </Right>
             </ListItem>
              
           </List>
         </Content>
        
       </ScrollView> 

          )}
                    enableEmptySections={true}
                    style={{ marginTop: 10 }}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    viewStyle: {
        justifyContent: 'center',
        flex: 1,
        backgroundColor: 'white',
        marginTop: Platform.OS == 'ios' ? 30 : 0
    },
    textStyle: {
        padding: 10,
    },
});
 